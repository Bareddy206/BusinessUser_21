const Joi = require('joi');
const Schema = require('../config/schema');

const tableName = `store_details`;
const optionalParams = ['MID','MERCHANT_KEY'];
const storeSchema = {
	hashKey: 'storeName',
	// rangeKey:'username',
	timestamps: true,
	schema: Joi.object({
		storeName: Joi.string(),
		//merchant_id:Joi.string()
		MID:Joi.string(),
		MERCHANT_KEY:Joi.string()
	}).unknown(true) //we will only allow unknown values in development
};
const Storename = Schema(storeSchema, {tableName:tableName});

module.exports = Storename;

//hash key - storename/emailid